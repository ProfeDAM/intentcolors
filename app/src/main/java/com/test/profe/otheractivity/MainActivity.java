package com.test.profe.otheractivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
    }




    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.button)
        {
            Intent intent = new Intent (this, DisplayMessageActivity.class);
            /* Ejemplo de intent explícito */
            //intent.putExtra
            EditText editText = (EditText) findViewById(R.id.editText);
            String message = editText.getText().toString();
            intent.putExtra("uncleBob", message);
        /* Recuperar el contenido del Spinner en un String
        y pasarlo a la DisplayMessageActivity
         */
            Spinner spinner = (Spinner) findViewById(R.id.spinner);
            String color = spinner.getSelectedItem().toString();

            intent.putExtra("color", color);
            startActivity(intent);

        }

    }
}
