package com.test.profe.otheractivity;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        String message = getIntent().getStringExtra("uncleBob");
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(message);
        // textView.append(message);

        /* Recuperar el valor del color del intent, y según el
        valor, modificar el color del Layout */
        String color = getIntent().getStringExtra("color");
        ConstraintLayout constraintLayout =
                (ConstraintLayout) findViewById(R.id.constraintLayout);
        switch (color)
        {
            case "Yellow": constraintLayout.setBackgroundColor(Color.YELLOW); break;
            case "Pink": constraintLayout.setBackgroundColor(Color.rgb(255,105,108)); break;
            case "Gray": constraintLayout.setBackgroundColor(Color.GRAY); break;

        }








    }
}
